﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SaketTestDotNetCore.Model;

namespace SaketTestDotNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {



        private DataContext db = new DataContext();

        [Produces("application/json")]
        [HttpGet("getAllUsers")]
        public ActionResult GetAllUser()
        {            
            try
            {      
                var customer =db.customer.ToList();
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [HttpGet("getUser/{Id}")]
        public ActionResult GetUserById( int Id)
        {
            try
            {
                var customer = db.customer.Find(Id);
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

         
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("Create")]
        public ActionResult CreateUser([FromBody]Customer _customer)
        {
            try
            {
                db.customer.Add(_customer);
                db.SaveChanges();
                var customer = db.customer.ToList();
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPut("Update")]
        public ActionResult UpdateUser([FromBody]Customer _customer)
        {
            try
            {
                db.Entry(_customer).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                var customer = db.customer.ToList();
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpDelete("Delete/{Id}")]
        public ActionResult DeleteUser(int Id)
        {
            try
            {
                db.customer.Remove(db.customer.Find(Id));
                db.SaveChanges();
                var customer = db.customer.ToList();
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}

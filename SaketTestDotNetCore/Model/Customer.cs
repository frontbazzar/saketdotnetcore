﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaketTestDotNetCore.Model
{
    public class Customer
    {

        [Key]
        public int LoginID { get; set; }
        public String LoginName { get; set; }
        public String LoginPwd { get; set; }
        public String EmailID { get; set; }
        public String DisplayName { get; set; }

                                    
    }
}
